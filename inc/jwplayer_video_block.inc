<?php
/**
@file
**/
function jwplayer_settings_form($form, &$form_state) {
  $number_of_blocks = array();
  for ($i = 1; $i <= 10; $i++) {
    $number_of_blocks[$i] = $i;
  }
  $form['number_of_blocks'] = array(
    '#title' => t('Number of JW Player Video blocks'),
    '#type' => 'select',
    '#options' => $number_of_blocks,
    '#default_value' => variable_get('number_of_blocks', 1),
    '#description' => t('Select number of blocks to be appeared in block section.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}
function jwplayer_settings_form_submit($form, &$form_state) {
  $number_block = $form_state['values']['number_of_blocks'];
  variable_set('number_of_blocks', $number_block);
  drupal_set_message(t("Block added successfully, Please goto  <a href='/admin/structure/block'>block setting page </a> to set JW Player Video blocks."));
}

function _jw_player_video_display($value='') {
  $result = db_select('jwplayer_player_srcipt', 'jw')
            ->fields('jw', array('player_script'))
            ->condition('player_id', $value)
            ->execute()
            ->fetchAll();

  foreach ($result as $array) {
    $scripttag = $array->player_script;
    $doc = new DOMDocument();
    @$doc->loadHTML($scripttag);
    $script = $doc->getElementsByTagName('script');
    if($script->length != 0){
      $script_src = $script->item(0)->getAttribute('src');
      $video_src = make_curl_url($script_src) ;
      $httpcode = make_curl_request($video_src);
      if ($httpcode != 200) {
        $video = "<center>Video not yet available.</center><hr />";  
      }
      else {
        $video = "<div><center>{$scripttag}</center></div>";
      }
    } else {
      $video = "<center>Video not yet available.</center><hr />";  
    }
  }
  $video_player = "";

  if (isset($video)) {
    $video_player = theme("video_player", array('data' => $video));
  }


  return $video_player;
}
function make_curl_url($url) {
  if ((strpos($url, "http:") == FALSE) && (strpos($url, "https:") == FALSE)) {
    $url = "https:" . $url;
  }
  $time = time();
  $url .= '?q=' . $time;

  return $url;
}
function make_curl_request($url) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_HEADER, 1);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_NOBODY, 1);
  $curl_output = curl_exec($ch);
  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close($ch);

  return $httpcode;
}