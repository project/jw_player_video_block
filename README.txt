
Description
-----------
This module provide easy integration of Jw Player video on drupal website.
This module supports features available in jw8.
This modules also check for expiration time of video before rendring on page itself.
It provides facility to create  10 Jw player video block (configurable in module file).
each block could be configured differently for different video script.


Installation
------------
1) Place this module directory in your "modules" folder (this will usually be
   "sites/all/modules/"). Don't install your module in Drupal core's "modules"
   folder, since that will cause problems and is bad practice in general. If
   "sites/all/modules" doesn't exist yet, just create it.

2) Enable the module.

3) Visit "admin/config/development/jwplayer" to learn about the various settings.


FAQ
---
Q: Is this module check for video expiration?
A: Yes.

Q: How to add script tag in block configuration?
A: It should be like <script src="//content.jwplatform.com/players/abCgdqLL-BaaaXeHs.js"></script>.

Q: From where i would get video script?
A: You need to create account on http://jwplayer.com.

Note: You need to create a jw player account.

Author
------
Mayank Kaushik ~ drupal User(mayank_kaushik)
